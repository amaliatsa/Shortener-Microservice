<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase
{
    /**
     * Test that the shortener route informs user that url is required in case missing
     */
    public function testPostShortenUrlWithoutBody(){
        $response = $this->runApp('POST', '/shorten',[]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('Url is missing!', (string)$response->getBody());
    }

    /**
     * Test that the shortener route will use the default provider in case provider is missing
     */
    public function testPostShortenUrlWithoutProvider(){
        $response = $this->runApp('POST', '/shorten',['longurl'=>'https://www.slimframework.com/']);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('http://bit.ly/2yr9fkM', (string)$response->getBody());
    }


    /**
     * Test that the shortener route will use the default provider in case user types invalid provider name
     */
    public function testPostShortenUrlWithWrongProviderName(){
        $response = $this->runApp('POST', '/shorten',['longurl'=>'https://www.slimframework.com/','provider'=>'googlebla']);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('http://bit.ly/2yr9fkM', (string)$response->getBody());
    }


    /**
     * Test that the shortener route returns Invalid url message
     */
    public function testPostShortenUrlWithWrongUrl(){
        $response = $this->runApp('POST', '/shorten',['longurl'=>'ramework.com/','provider'=>'googlebla']);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('INVALID_URI', (string)$response->getBody());
    }

    /**
     * Test that the shortener route won't accept a get request
     */
    public function testGetShortenRequestNotAllowed(){
        $response = $this->runApp('GET', '/shorten');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('<html>', (string)$response->getBody());
    }


}