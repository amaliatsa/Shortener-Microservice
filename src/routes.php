<?php

use Slim\Http\Request;
use Slim\Http\Response;

require_once 'Apis/Api.php';


/**
 * Route  for POST request, path: '/shorten' that returns the shortened url using a third-party api to shorten it.
 * @return Response
 */
$app->post('/shorten', function (Request $request, Response $response){

    $data = $request->getParsedBody();
    //validate  and fix input data
    if(array_key_exists('url',$data)) {
        $long_url = $data["url"];
    } else{
        return $response ->getBody()->write("Url is missing!");
    }
    if(array_key_exists('provider',$data)&& (strtolower($data["provider"])=='bit.ly' ||strtolower($data["provider"])=='goo.gl' )) {
        $provider= strtolower($data["provider"]);
    } else {
        $provider= "bit.ly";
    }

    $s_api = new Api($provider);
    $short_Url = $s_api ->shorten($long_url);
    $response ->getBody()->write("Provider: $provider , Shortened url: $short_Url");
    return $response;
});


