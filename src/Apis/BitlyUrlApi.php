<?php

include_once 'iShortenUrlApi.php';

class BitlyUrlApi implements iShortenUrlApi
{

    private $keyUrl='';
    private $apiURL = 'http://api.bitly.com/v3/shorten';
    private $credentials = [];

    /**
     * @param Array $credentials : The api key and login.
     * BitlyUrlApi class constructor instatiate an object that encaptulates Bit.ly api.
     * @return BitlyUrlApi Object
     */
    function BitlyUrlApi($credentials){
        $this->credentials = $credentials;
        $this->buildCredUrl();
    }


    /**
     * Method that builds part of url with credentials.
     * @return void
     */
    function buildCredUrl()
    {
        // Keep the API Url
        $this->keyUrl = $this->apiURL . '?apiKey=' . $this->credentials[0]."&login=".$this->credentials[1];
    }

    /**
     * @param String $url : The long url to shorten.
     * Method that use the api to shorten a url and returns a string shortened url on success and a string error on failure.
     * @return String
     */
    function shorten($url)
    {

        $api_call = file_get_contents($this->keyUrl."&longUrl=".$url);

        $bitlyinfo=json_decode(utf8_encode($api_call),true);

        if ($bitlyinfo['status_code'] == 200)
        {
            return $bitlyinfo['data']['url'];
        }
        else
        {
            return "ERROR " . $bitlyinfo['status_code']. ": " .$bitlyinfo['status_txt'];
        }
    }

}

?>