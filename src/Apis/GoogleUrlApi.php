<?php
/**
 * Created by IntelliJ IDEA.
 * User: kater
 * Date: 17/10/2018
 * Time: 20:49
 */

include_once 'iShortenUrlApi.php';

class GoogleUrlApi implements iShortenUrlApi
{
    private $keyURL='';
    private $apiURL = 'https://www.googleapis.com/urlshortener/v1/url';
    private $credentials =[];

    /**
     * @param Array $credentials : The google api key.
     * GoogleUrlApi class constructor instatiates an object that encaptulates goo.gl api.
     * @return GoogleUrlApi Object
     */
    function GoogleUrlApi($credentials){
        $this->credentials=$credentials;
        $this->buildCredUrl();
    }

    /**
     * @param Array $credentials : The api key.
     * Method that builds part of url with credentials.
     * @return void
     */
    function buildCredUrl()
    {
        // Keep the API Url
        $this->keyURL = $this->apiURL . '?apiKey=' . $this->credentials[0];
    }

    /**
     * @param String $url : The long url to shorten.
     * Method that use the api to shorten a url and returns a string shortened url on success and a string error on failure.
     * @return Sting
     */
    function shorten($url)
    {
        // Send information along
        $response = $this->send($url);

        // Return the result
        return isset($response['id']) ? $response['id'] : "Error creating goo.gl url!";
    }


    /**
     * @param String $url : The long url to shorten.
     * Create request for goo.gl api.
     **/
    function send($url)
    {
        // Create cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->keyURL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("longUrl" => $url)));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // Execute the post
        $result = curl_exec($ch);
        // Close the connection
        curl_close($ch);
        // Return the result
        return json_decode($result, true);
    }
}
?>