<?php
/**
 * Created by IntelliJ IDEA.
 * User: amalia
 * Date: 18/10/2018
 * Time: 02:55
 */

/**
 * Interface iShortenUrlApi, provide abstact methods for shortener apis.
 */
interface iShortenUrlApi{

    function buildCredUrl();

    function shorten($url);

}