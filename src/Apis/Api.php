<?php
/**
 * Created by IntelliJ IDEA.
 * User: kater
 * Date: 18/10/2018
 * Time: 23:06
 */


require_once 'GoogleUrlApi.php';
require_once 'BitlyUrlApi.php';

/**
 * Select shortener api according to provider name
 * */
class Api {

    private $api = NULL;
    private $provider = "";

    /**
     * @param Sting $provider : The shortener api name.
     * Api class constructor instatiate shortener api object according to provider value.
     * @return Api Object
     */
    public function __construct($provider) {

        $this->provider = $provider;

        switch ($provider) {
            case "bit.ly":
                $this->api= new BitlyUrlApi($this->getApiCredentials($provider));
                break;
            case "goo.gl":
                $this->api = new GoogleUrlApi($this->getApiCredentials($provider));
                break;
        }
    }

    /**
     * @param String $url : the long url to short.
     * This method that calls the api shorter method and returns the shortened url.
     * */
    public function shorten($url) {
            return $this->api->shorten($url);
    }


    /**
     * @param String $provider : The api name given by user.
     * Given the provider name returns an array with api credentials.
     * @return Array
     */
    public function getApiCredentials($provider){

        if($provider=="goo.gl"){
            return ["AIzaSyBtoPLsN02cr-oErJVyKteiPMjb-bodl1g"];
        } else if($provider=="bit.ly") {
            return ["R_58268d71caac452dbdf9921f95e6da77","o_18ltbu251t"];
        }

    }

}
?>